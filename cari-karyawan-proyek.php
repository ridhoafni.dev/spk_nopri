<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Cari Data Karyawan Proyek</h3>
	</div>
<?php
if($page=='form'){
?>
<p><br/></p>
</div>
	<p></p>
<?php
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->
				<?php
				
				$id_kemajuan_proyek 	    = $_GET['id'];				
				$id_data_proyek 			= $_POST['proyek'];
				$kemajuan 					= $_POST['kemajuan'];
				$tanggal 					= $_POST['tanggal'];
				$user_id 					= $_SESSION['username'];
			
				$stmt = $db->prepare("update smart_kemajuan_proyek set data_proyek_id=?, kemajuan=?, tanggal=? where id_kemajuan_proyek=?");

				$stmt->bindParam(1,$id_data_proyek);
				$stmt->bindParam(2,$kemajuan);
				$stmt->bindParam(3,$tanggal);
				$stmt->bindParam(4,$id_kemajuan_proyek);
				if($stmt->execute()){
					?>
					<script type="text/javascript">location.href='kemajuan-data-proyek.php'</script>
					<?php
				} else{
					?>
					<script type="text/javascript">alert("Gagal Menyimpan data")</script>
					<?php
				}
		}
	?>
<?php
}else{
?>
</div>
<!-- Load File javascript config.js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/config.js" type="text/javascript"></script>
	<form method="post">
		<input type="hidden" name="id_kemajuan_proyek" value="<?php echo isset($_GET['id'])? $_GET['id'] : ''; ?>">
        <?php
            //Get all country data
            $query = $db->prepare("select * from smart_data_proyek ORDER BY nama_proyek ASC");
            
            //Count total number of rows
            $query->execute();

            $rowCount = $query->fetch();
        ?>
		<label>Jenis Proyek</label>
		<div class="input-control text full-size">
			<select name="proyek">
		    	<option value="<?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?>"><?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?></option>
		    	<?php
				$stmt3 = $db->prepare("select * from smart_data_proyek");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
		    	<option value="<?php echo $row3['jenis_proyek'] ?>"><?php echo $row3['jenis_proyek'] ?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</div>

        <label>Karyawan</label>
		<div class="input-control text full-size">
			<select name="karyawan">
		    	<option value="<?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?>"><?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?></option>
		    	<?php
				$stmt3 = $db->prepare("select * from smart_karyawan");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
		    	<option value="<?php echo $row3['id_karyawan'] ?>"><?php echo $row3['nama_karyawan'] ?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</div>

		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="cari" class="button primary">Cari</button>
			<?php
		}
		?>
	</form>

	<?php
	if(isset($_POST['cari'])){

		$proyek 	= $_POST['proyek'];
        $karyawan 	= $_POST['karyawan'];
        
        $stmt_cari = $db->prepare(" SELECT
            smart_karyawan.*,
            smart_karyawan_proyek.*,
            smart_data_proyek.*
        FROM
            smart_karyawan_proyek
            INNER JOIN smart_data_proyek ON smart_data_proyek.id_data_proyek = smart_karyawan_proyek.proyek_id
            INNER JOIN smart_karyawan ON smart_karyawan_proyek.karyawan_id = smart_karyawan.id_karyawan
            WHERE smart_karyawan_proyek.karyawan_id=? AND smart_data_proyek.jenis_proyek=?");
        $stmt_cari->bindParam(1,$karyawan);
        $stmt_cari->bindParam(2,$proyek);
        if($stmt_cari->execute()){
        ?>
		<div class="cell colspan2 align-right">
		</div>
		<!-- <table class="table striped hovered cell-hovered border bordered dataTable">
		<thead>
			<tr>
				<th width="50">No.</th>
				<th>Nama Proyek</th>
				<th>Karyawan</th>
				<th width="240">Aksi</th> -->
			<!-- </tr> -->
		<!-- </thead> -->
		<!-- <tbody> -->
			<?php 
			//$no = 1;
			//print_r($stmt_cari->fetch());
			//while($rows = $stmt_cari->fetch()){
			?>
			<!-- <tr> -->
				<!-- <td><?php //echo $no++ ?></td> -->
				<!-- <td><?php //echo $rows['nama_proyek'] ?></td> -->
				<!-- <td><?php //echo $rows['nama_karyawan'] ?></td> -->
			<!-- </tr> -->
			<?php
			//}
			?>
		<!-- </tbody> -->
		<!-- </table> -->

		<table class="table striped hovered cell-hovered border bordered dataTable">
		<thead>
			<tr>
				<th width="50">No</th>
				<th>Alternatif</th>
				<th>Proyek</th>
				<?php
				$stmt2 = $db->prepare("select * from smart_kriteria");
				$stmt2->execute();
				while($row2 = $stmt2->fetch()){
				?>
				<th><?php echo $row2['nama_kriteria'] ?></th>
				<?php
				}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
			$stmt = $db->prepare("select smart_alternatif.*, smart_karyawan.*, smart_karyawan_proyek.*,
            smart_data_proyek.* 
			from smart_alternatif
			INNER JOIN smart_karyawan ON smart_alternatif.nama_alternatif = smart_karyawan.id_karyawan
			INNER JOIN smart_karyawan_proyek ON smart_karyawan_proyek.karyawan_id = smart_karyawan.id_karyawan 
			INNER JOIN smart_data_proyek ON smart_data_proyek.id_data_proyek = smart_karyawan_proyek.proyek_id 
			where smart_karyawan.id_karyawan=?");
			$stmt->bindParam(1,$karyawan);
			$nox = 1;
			$stmt->execute();
			while($row = $stmt->fetch()){
			?>
			<tr>
				<td><?php echo $nox++ ?></td>
				<td><?php echo $row['nama_karyawan'] ?></td>
				<td><?php echo $row['nama_proyek'] ?></td>
				<?php
				$stmt3 = $db->prepare("select * from smart_kriteria");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
					// $stmt5 = $db->prepare("select * from smart_dkriteria where id_kriteria = '".$row3['id_kriteria']."' ");
					// $stmt5->execute();
					// while($row5 = $stmt5->fetch()){
				?>
				<td>
					<?php
					$stmt4 = $db->prepare("select * from smart_alternatif_kriteria where id_kriteria='".$row3['id_kriteria']."' and id_alternatif='".$row['id_alternatif']."'");
					$stmt4->execute();
					while($row4 = $stmt4->fetch()){
						echo $row4['nilai_alternatif_kriteria'];
						?>
						<!--<a href="?page=form&alt=<?php echo $row['id_alternatif'] ?>&kri=<?php echo $row3['id_kriteria'] ?>&nilai=<?php echo $row4['nilai_alternatif_kriteria'] ?>" style="color:orange"><span class="mif-pencil icon"></span></a>-->
						<?php
					}
					?>
				</td>
				<?php
				}
			// }
				?>
			</tr>
			<?php
			}
			?>
		</tbody>
		</table>

        <?php
        	}else{
        ?>
        <script type="text/javascript">alert('Gagal menyimpan data')</script>
        <?php
        }		
	}
}
include "footer.php";
?>
				