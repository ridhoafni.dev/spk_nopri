<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Kemajuan Data Proyek</h3>
	</div>
<?php
if($page=='form'){
?>
	<div class="cell colspan2 align-right">
		<a href="kemajuan-data-proyek.php" class="button info">Kembali</a>
	</div>
</div>
	<p></p>
	<?php
	if(isset($_POST['simpan'])){

		$id_data_proyek 			= $_POST['proyek'];
		$kemajuan 					= $_POST['kemajuan'];
		$tanggal 					= $_POST['tanggal'];
		$user_id 					= $_SESSION['username'];
		
		$stmt 						= $db->prepare("insert into smart_kemajuan_proyek values('',?,?,?,?)");
		$stmt->bindParam(1,$id_data_proyek);
		$stmt->bindParam(2,$kemajuan);
		$stmt->bindParam(3,$tanggal);
		$stmt->bindParam(4,$user_id);
		if($stmt->execute()){
			?>
			<script type="text/javascript">location.href='kemajuan-data-proyek.php'</script>
			<?php
		} else{
			?>
			<script type="text/javascript">alert('Gagal menyimpan data')</script>
			<?php
		}
				
	}
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->
				<?php
				
				$id_kemajuan_proyek 	    = $_GET['id'];				
				$id_data_proyek 			= $_POST['proyek'];
				$kemajuan 					= $_POST['kemajuan'];
				$tanggal 					= $_POST['tanggal'];
				$user_id 					= $_SESSION['username'];
			
				$stmt = $db->prepare("update smart_kemajuan_proyek set data_proyek_id=?, kemajuan=?, tanggal=? where id_kemajuan_proyek=?");

				$stmt->bindParam(1,$id_data_proyek);
				$stmt->bindParam(2,$kemajuan);
				$stmt->bindParam(3,$tanggal);
				$stmt->bindParam(4,$id_kemajuan_proyek);
				if($stmt->execute()){
					?>
					<script type="text/javascript">location.href='kemajuan-data-proyek.php'</script>
					<?php
				} else{
					?>
					<script type="text/javascript">alert("Gagal Menyimpan data")</script>
					<?php
				}
		}
	?>
	<!-- Load File javascript config.js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/config.js" type="text/javascript"></script>
	<form method="post">
		<input type="hidden" name="id_kemajuan_proyek" value="<?php echo isset($_GET['id'])? $_GET['id'] : ''; ?>">
        <?php
            //Get all country data
            $query = $db->prepare("select * from smart_data_proyek ORDER BY nama_proyek ASC");
            
            //Count total number of rows
            $query->execute();

            $rowCount = $query->fetch();
        ?>
		<label>Data Proyek</label>
		<div class="input-control text full-size">
			<select name="proyek" id="proyek">
		    	<option value="<?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?>"><?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?></option>
		    	<?php
				$stmt3 = $db->prepare("select * from smart_data_proyek");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
		    	<option value="<?php echo $row3['id_data_proyek'] ?>"><?php echo $row3['nama_proyek'] ?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</div>
        <label>Nama Kontraktor</label>
		<div class="input-control text full-size">
		<select name="kontraktor" id="kontraktor">
            <option value="">Pilih kontraktor</option>
        </select>
		</div>

		<div id="loading" style="margin-top: 15px;">
          <img src="assets/loading/loading.gif" width="18"> <small>Loading...</small>
        </div>
        
        <label>Kemajuan Proyek</label>
		<div class="input-control text full-size">
		    <input type="number" name="kemajuan" placeholder="Kemajuan Proyek" value="<?php echo isset($_GET['kemajuan'])? $_GET['kemajuan'] : ''; ?>">
		</div>

		<label>Tanggal</label>
		<div class="input-control text full-size">
		    <input type="date" name="tanggal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal'])? $_GET['tanggal'] : ''; ?>">
		</div>

		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="simpan" class="button primary">Simpan</button>
			<?php
		}
		?>
	</form>
<?php
} else if($page=='hapus'){
?>
	<div class="cell colspan2 align-right">
	</div>
</div>
<?php
	if(isset($_GET['id'])){
		$stmt = $db->prepare("delete from smart_kemajuan_proyek where id_kemajuan_proyek ='".$_GET['id']."'");
	 	if($stmt->execute()){
	 		?>
	 		<script type="text/javascript">location.href='kemajuan-data-proyek.php'</script>
	 		<?php
	 	}
	}
} else{
?>
	<div class="cell colspan2 align-right">
		<a href="?page=form" class="button primary">Tambah</a>
	</div>
</div>
<table class="table striped hovered cell-hovered border bordered dataTable" data-role="datatable" data-searching="true">
	<thead>
		<tr>
			<th width="50">ID</th>
			<th>Nama Proyek</th>
			<th>Nama Kontraktor</th>
			<th>Kemajuan</th>
			<th width="50">Pengawas Lapangan</th>
			<th width="50">Waktu Mulai (Awal)</th>
			<th width="50">Waktu Selesai (Akhir)</th>
			<th width="50">Jenis Proyek</th>
			<th width="50">Jumlah Pekerja</th>
			<th width="50">Gambar</th>
			<th width="50">Status</th>
			<th width="240">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
        $stmt = $db->prepare("select smart_data_proyek.*, smart_kemajuan_proyek.* 
                              from smart_kemajuan_proyek
                              INNER JOIN smart_data_proyek ON smart_data_proyek.id_data_proyek = 
                              smart_kemajuan_proyek.data_proyek_id");
		$stmt->execute();
        $no = 1;
		while($row = $stmt->fetch()){
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $row['nama_proyek'] ?></td>
			<td><?php echo $row['nama_kontraktor'] ?></td>
			<td><?php echo $row['kemajuan'] ?> %</td>
			<td><?php echo $row['pengawas_lapangan'] ?></td>
			<td><?php echo $row['waktu_pelaksanaan_awal'] ?></td>
			<td><?php echo $row['waktu_pelaksanaan_akhir'] ?></td>
			<td><?php echo $row['jenis_proyek'] ?></td>
			<td><?php echo $row['jumlah_pekerja'] ?> Orang</td>
			<td><img src="assets/gambar/<?= $row['gambar'] ?>" height="100px" width="100px;"></td>
			<td><?php echo $row['status'] ?></td>
			<td class="align-center">
				<a href="?page=form&id=<?php echo $row['id_kemajuan_proyek'] ?>&id_data_proyek=<?php echo $row['data_proyek_id'] ?>&kemajuan=<?php echo $row['kemajuan'] ?>&tanggal=<?php echo $row['tanggal'] ?>" ><span class="mif-pencil icon"></span> Edit</a>
				 | <a href="?page=hapus&id=<?php echo $row['id_kemajuan_proyek'] ?>"><span class="mif-cancel icon"></span> Hapus</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<p><br/></p>
<?php
}
include "footer.php";
?>
					
					