-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2019 at 03:24 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spk_smart`
--

-- --------------------------------------------------------

--
-- Table structure for table `smart_admin`
--

CREATE TABLE IF NOT EXISTS `smart_admin` (
`id_admin` int(11) NOT NULL,
  `nama_admin` varchar(80) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','pimpinan','koordinator lapangan','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_admin`
--

INSERT INTO `smart_admin` (`id_admin`, `nama_admin`, `username`, `password`, `level`) VALUES
(3, 'Rifaldi Saputra', 'rifaldi', 'e08c6b928ac3bc872ab82de57fa97ba8', 'admin'),
(4, 'pimpinan', 'pimpinan', '90973652b88fe07d05a4304f0a945de8', 'pimpinan'),
(5, 'Koordinator Lapangan', 'koordinator', '6620a171fdf7ec04d322421994858c21', 'koordinator lapangan');

-- --------------------------------------------------------

--
-- Table structure for table `smart_alternatif`
--

CREATE TABLE IF NOT EXISTS `smart_alternatif` (
`id_alternatif` int(11) NOT NULL,
  `nama_alternatif` varchar(45) NOT NULL,
  `nilai_utility` double NOT NULL,
  `hasil_alternatif` double NOT NULL,
  `ket_alternatif` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_alternatif`
--

INSERT INTO `smart_alternatif` (`id_alternatif`, `nama_alternatif`, `nilai_utility`, `hasil_alternatif`, `ket_alternatif`) VALUES
(14, 'Irwansyah', 0, 84, 'Sangat Layak'),
(15, 'Habibi', 0, 78, 'Layak');

-- --------------------------------------------------------

--
-- Table structure for table `smart_alternatif_kriteria`
--

CREATE TABLE IF NOT EXISTS `smart_alternatif_kriteria` (
  `id_alternatif` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `nilai_alternatif_kriteria` double NOT NULL,
  `bobot_alternatif_kriteria` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_alternatif_kriteria`
--

INSERT INTO `smart_alternatif_kriteria` (`id_alternatif`, `id_kriteria`, `nilai_alternatif_kriteria`, `bobot_alternatif_kriteria`) VALUES
(0, 18, 100, 0),
(0, 19, 100, 0),
(0, 20, 100, 0),
(0, 21, 100, 0),
(9, 18, 60, 18),
(9, 19, 40, 10),
(9, 20, 10, 1),
(9, 21, 60, 21),
(10, 18, 60, 18),
(10, 19, 80, 20),
(10, 20, 40, 4),
(10, 21, 10, 3.5),
(11, 18, 40, 12),
(11, 19, 60, 15),
(11, 20, 60, 6),
(11, 21, 60, 21),
(12, 18, 80, 24),
(12, 19, 100, 25),
(12, 20, 60, 6),
(12, 21, 60, 21),
(13, 18, 80, 24),
(13, 19, 100, 25),
(13, 20, 60, 6),
(13, 21, 100, 35),
(14, 18, 80, 24),
(14, 19, 60, 15),
(14, 20, 100, 10),
(14, 21, 100, 35),
(15, 18, 80, 24),
(15, 19, 80, 20),
(15, 20, 60, 6),
(15, 21, 80, 28);

-- --------------------------------------------------------

--
-- Table structure for table `smart_data_proyek`
--

CREATE TABLE IF NOT EXISTS `smart_data_proyek` (
`id_data_proyek` int(11) NOT NULL,
  `nama_proyek` varchar(255) NOT NULL,
  `nama_kontraktor` varchar(100) NOT NULL,
  `jenis_proyek` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `pengawas_lapangan` varchar(100) NOT NULL,
  `waktu_pelaksanaan_awal` date NOT NULL,
  `waktu_pelaksanaan_akhir` date NOT NULL,
  `jumlah_pekerja` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `status` enum('Selesai','Tidak Selesai','Target Tidak Tepat Waktu','Sedang Jalan','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_data_proyek`
--

INSERT INTO `smart_data_proyek` (`id_data_proyek`, `nama_proyek`, `nama_kontraktor`, `jenis_proyek`, `alamat`, `pengawas_lapangan`, `waktu_pelaksanaan_awal`, `waktu_pelaksanaan_akhir`, `jumlah_pekerja`, `gambar`, `status`) VALUES
(11, 'pembangunan update lagi', 'azis', 'Perumahan', 'pkufdfsfgsfgspkufdfsfgs', 'kamto', '2019-11-01', '2019-11-14', 60, '18161650_1899665673585469_6839115874142519296_n.jpg', 'Sedang Jalan'),
(12, 'test', 'test', 'Perumahan', 'test', 'test', '2019-11-02', '2019-11-30', 90, '69185305_101649694537500_464112228975509504_n.jpg', 'Sedang Jalan');

-- --------------------------------------------------------

--
-- Table structure for table `smart_karyawan`
--

CREATE TABLE IF NOT EXISTS `smart_karyawan` (
`id_karyawan` int(11) NOT NULL,
  `nama_karyawan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `bidang_keahlian` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_karyawan`
--

INSERT INTO `smart_karyawan` (`id_karyawan`, `nama_karyawan`, `alamat`, `jabatan`, `email`, `no_hp`, `bidang_keahlian`) VALUES
(2, 'Asrul puadi', 'simpang kanan\r\nbukit datuk', 'teknik informatika', 'Syakir.alvaro@gmail.com', '082252416420', 'Pemasangan Pintu');

-- --------------------------------------------------------

--
-- Table structure for table `smart_kemajuan_proyek`
--

CREATE TABLE IF NOT EXISTS `smart_kemajuan_proyek` (
`id_kemajuan_proyek` int(11) NOT NULL,
  `data_proyek_id` int(11) NOT NULL,
  `kemajuan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_kemajuan_proyek`
--

INSERT INTO `smart_kemajuan_proyek` (`id_kemajuan_proyek`, `data_proyek_id`, `kemajuan`, `tanggal`, `user_id`) VALUES
(1, 11, 60, '2019-11-19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `smart_kriteria`
--

CREATE TABLE IF NOT EXISTS `smart_kriteria` (
`id_kriteria` int(11) NOT NULL,
  `nama_kriteria` varchar(45) NOT NULL,
  `bobot_kriteria` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_kriteria`
--

INSERT INTO `smart_kriteria` (`id_kriteria`, `nama_kriteria`, `bobot_kriteria`) VALUES
(18, 'Analisa', 0.3),
(19, 'Matematika', 0.25),
(20, 'Program', 0.1),
(21, 'Etika', 0.35);

-- --------------------------------------------------------

--
-- Table structure for table `smart_status_proyek`
--

CREATE TABLE IF NOT EXISTS `smart_status_proyek` (
`id_status_proyek` int(11) NOT NULL,
  `data_proyek_id` int(11) DEFAULT NULL,
  `waktu_tanggal` date DEFAULT NULL,
  `kendala` varchar(255) DEFAULT 'Tidak ada',
  `penyebab_kendala` varchar(255) DEFAULT 'Tidak ada'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_status_proyek`
--

INSERT INTO `smart_status_proyek` (`id_status_proyek`, `data_proyek_id`, `waktu_tanggal`, `kendala`, `penyebab_kendala`) VALUES
(10, 11, '2019-11-19', 'tidak ada haha', 'tidak ada haha');

-- --------------------------------------------------------

--
-- Table structure for table `smart_sub_kriteria`
--

CREATE TABLE IF NOT EXISTS `smart_sub_kriteria` (
`id_sub_kriteria` int(11) NOT NULL,
  `nama_sub_kriteria` varchar(45) NOT NULL,
  `nilai_sub_kriteria` double NOT NULL,
  `id_kriteria` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_sub_kriteria`
--

INSERT INTO `smart_sub_kriteria` (`id_sub_kriteria`, `nama_sub_kriteria`, `nilai_sub_kriteria`, `id_kriteria`) VALUES
(39, 'Sangat Baik', 100, 18),
(40, 'Baik', 80, 18),
(41, 'Cukup', 60, 18),
(42, 'Kurang', 40, 18),
(43, 'Sangat Kurang', 10, 18),
(45, 'Sangat Bagus', 100, 19),
(46, 'Bagus', 80, 19),
(47, 'Cukup', 60, 19),
(48, 'Kurang', 40, 19),
(49, 'Sangat Kurang', 10, 19),
(50, 'Sangat Baik', 100, 20),
(52, 'Cukup', 60, 20),
(53, 'Kurang', 40, 20),
(54, 'Sangat Kurang', 10, 20),
(60, 'Sangat Baik', 100, 21),
(61, 'Baik', 80, 21),
(62, 'Cukup', 60, 21),
(63, 'Kurang', 40, 21),
(64, 'Sangat Kurang', 10, 21),
(65, 'Baik', 80, 20);

-- --------------------------------------------------------

--
-- Table structure for table `smart_user`
--

CREATE TABLE IF NOT EXISTS `smart_user` (
`id_user` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `smart_admin`
--
ALTER TABLE `smart_admin`
 ADD PRIMARY KEY (`id_admin`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `smart_alternatif`
--
ALTER TABLE `smart_alternatif`
 ADD PRIMARY KEY (`id_alternatif`);

--
-- Indexes for table `smart_alternatif_kriteria`
--
ALTER TABLE `smart_alternatif_kriteria`
 ADD PRIMARY KEY (`id_alternatif`,`id_kriteria`);

--
-- Indexes for table `smart_data_proyek`
--
ALTER TABLE `smart_data_proyek`
 ADD PRIMARY KEY (`id_data_proyek`);

--
-- Indexes for table `smart_karyawan`
--
ALTER TABLE `smart_karyawan`
 ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `smart_kemajuan_proyek`
--
ALTER TABLE `smart_kemajuan_proyek`
 ADD PRIMARY KEY (`id_kemajuan_proyek`);

--
-- Indexes for table `smart_kriteria`
--
ALTER TABLE `smart_kriteria`
 ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `smart_status_proyek`
--
ALTER TABLE `smart_status_proyek`
 ADD PRIMARY KEY (`id_status_proyek`);

--
-- Indexes for table `smart_sub_kriteria`
--
ALTER TABLE `smart_sub_kriteria`
 ADD PRIMARY KEY (`id_sub_kriteria`);

--
-- Indexes for table `smart_user`
--
ALTER TABLE `smart_user`
 ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `smart_admin`
--
ALTER TABLE `smart_admin`
MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `smart_alternatif`
--
ALTER TABLE `smart_alternatif`
MODIFY `id_alternatif` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `smart_data_proyek`
--
ALTER TABLE `smart_data_proyek`
MODIFY `id_data_proyek` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `smart_karyawan`
--
ALTER TABLE `smart_karyawan`
MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `smart_kemajuan_proyek`
--
ALTER TABLE `smart_kemajuan_proyek`
MODIFY `id_kemajuan_proyek` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `smart_kriteria`
--
ALTER TABLE `smart_kriteria`
MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `smart_status_proyek`
--
ALTER TABLE `smart_status_proyek`
MODIFY `id_status_proyek` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `smart_sub_kriteria`
--
ALTER TABLE `smart_sub_kriteria`
MODIFY `id_sub_kriteria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `smart_user`
--
ALTER TABLE `smart_user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
