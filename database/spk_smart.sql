-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2019 at 11:36 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk_smart`
--

-- --------------------------------------------------------

--
-- Table structure for table `smart_admin`
--

CREATE TABLE `smart_admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(80) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','pimpinan','koordinator lapangan','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_admin`
--

INSERT INTO `smart_admin` (`id_admin`, `nama_admin`, `username`, `password`, `level`) VALUES
(3, 'Rifaldi Saputra', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(4, 'pimpinan', 'pimpinan', '90973652b88fe07d05a4304f0a945de8', 'pimpinan'),
(5, 'Koordinator Lapangan', 'koordinator', '6620a171fdf7ec04d322421994858c21', 'koordinator lapangan');

-- --------------------------------------------------------

--
-- Table structure for table `smart_alternatif`
--

CREATE TABLE `smart_alternatif` (
  `id_alternatif` int(11) NOT NULL,
  `nama_alternatif` varchar(45) NOT NULL,
  `nilai_utility` double NOT NULL,
  `hasil_alternatif` double NOT NULL,
  `ket_alternatif` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_alternatif`
--

INSERT INTO `smart_alternatif` (`id_alternatif`, `nama_alternatif`, `nilai_utility`, `hasil_alternatif`, `ket_alternatif`) VALUES
(19, '2', 0, 106.25, 'Sangat Layak'),
(20, '5', 0, 83.75, 'Sangat Layak');

-- --------------------------------------------------------

--
-- Table structure for table `smart_alternatif_kriteria`
--

CREATE TABLE `smart_alternatif_kriteria` (
  `id_alternatif` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `nilai_alternatif_kriteria` double NOT NULL,
  `bobot_alternatif_kriteria` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_alternatif_kriteria`
--

INSERT INTO `smart_alternatif_kriteria` (`id_alternatif`, `id_kriteria`, `nilai_alternatif_kriteria`, `bobot_alternatif_kriteria`) VALUES
(0, 18, 100, 0),
(0, 19, 100, 0),
(0, 20, 100, 0),
(0, 21, 100, 0),
(9, 18, 60, 18),
(9, 19, 40, 10),
(9, 20, 10, 1),
(9, 21, 60, 21),
(10, 18, 60, 18),
(10, 19, 80, 20),
(10, 20, 40, 4),
(10, 21, 10, 3.5),
(11, 18, 40, 12),
(11, 19, 60, 15),
(11, 20, 60, 6),
(11, 21, 60, 21),
(12, 18, 80, 24),
(12, 19, 100, 25),
(12, 20, 60, 6),
(12, 21, 60, 21),
(13, 18, 80, 24),
(13, 19, 100, 25),
(13, 20, 60, 6),
(13, 21, 100, 35),
(14, 18, 80, 24),
(14, 19, 60, 15),
(14, 20, 100, 30),
(14, 21, 100, 35),
(15, 18, 80, 24),
(15, 19, 80, 20),
(15, 20, 60, 18),
(15, 21, 80, 28),
(16, 18, 100, 30),
(16, 19, 50, 12.5),
(16, 20, 50, 15),
(16, 21, 50, 17.5),
(17, 18, 100, 30),
(17, 19, 75, 18.75),
(17, 20, 75, 22.5),
(17, 21, 50, 17.5),
(18, 18, 75, 22.5),
(18, 19, 75, 18.75),
(18, 20, 75, 22.5),
(18, 21, 100, 35),
(19, 18, 75, 22.5),
(19, 19, 75, 18.75),
(19, 20, 100, 30),
(19, 21, 100, 35),
(20, 18, 75, 22.5),
(20, 19, 50, 12.5),
(20, 20, 75, 22.5),
(20, 21, 75, 26.25);

-- --------------------------------------------------------

--
-- Table structure for table `smart_data_proyek`
--

CREATE TABLE `smart_data_proyek` (
  `id_data_proyek` int(11) NOT NULL,
  `nama_proyek` varchar(255) NOT NULL,
  `nama_kontraktor` varchar(100) NOT NULL,
  `jenis_proyek` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `pengawas_lapangan` varchar(100) NOT NULL,
  `waktu_pelaksanaan_awal` date NOT NULL,
  `waktu_pelaksanaan_akhir` date NOT NULL,
  `jumlah_pekerja` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `status` enum('Selesai','Tidak Selesai','Target Tidak Tepat Waktu','Sedang Jalan','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_data_proyek`
--

INSERT INTO `smart_data_proyek` (`id_data_proyek`, `nama_proyek`, `nama_kontraktor`, `jenis_proyek`, `alamat`, `pengawas_lapangan`, `waktu_pelaksanaan_awal`, `waktu_pelaksanaan_akhir`, `jumlah_pekerja`, `gambar`, `status`) VALUES
(14, 'dhfhfsjdf', 'dfjakdfa', 'Jalan', 'fhakdhfa', 'fadfhald', '2019-12-14', '2019-12-31', 44, 'tas05.jpg', 'Sedang Jalan'),
(15, 'Proyek2', 'Amin', 'Perumahan', 'PKU', 'Iyul', '2019-12-19', '2019-12-31', 20, 'haji4.jpg', 'Sedang Jalan');

-- --------------------------------------------------------

--
-- Table structure for table `smart_karyawan`
--

CREATE TABLE `smart_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nama_karyawan` varchar(100) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `bidang_keahlian` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_karyawan`
--

INSERT INTO `smart_karyawan` (`id_karyawan`, `nama_karyawan`, `photo`, `alamat`, `jabatan`, `email`, `no_hp`, `bidang_keahlian`) VALUES
(2, 'Asrul puadi', 'Koala.jpg', 'simpang kananbukit datuk', 'teknik informatika', 'Syakir.alvaro@gmail.com', '082252416420', 'Pemasangan Pintu'),
(5, 'Suci', '91bdd85d-00ef-499a-add4-43e0fdc5c60a.jpg', 'pku', 'teknisi', 'nor1095@yahoo.com', '0822', 'Pemasangan Pintu');

-- --------------------------------------------------------

--
-- Table structure for table `smart_karyawan_proyek`
--

CREATE TABLE `smart_karyawan_proyek` (
  `id` int(11) NOT NULL,
  `proyek_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `smart_karyawan_proyek`
--

INSERT INTO `smart_karyawan_proyek` (`id`, `proyek_id`, `karyawan_id`) VALUES
(1, 15, 2),
(2, 15, 5),
(3, 14, 2),
(4, 14, 5);

-- --------------------------------------------------------

--
-- Table structure for table `smart_kemajuan_proyek`
--

CREATE TABLE `smart_kemajuan_proyek` (
  `id_kemajuan_proyek` int(11) NOT NULL,
  `data_proyek_id` int(11) NOT NULL,
  `kemajuan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_kemajuan_proyek`
--

INSERT INTO `smart_kemajuan_proyek` (`id_kemajuan_proyek`, `data_proyek_id`, `kemajuan`, `tanggal`, `user_id`) VALUES
(1, 11, 60, '2019-11-19', 0),
(2, 15, 20, '2019-12-20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `smart_kriteria`
--

CREATE TABLE `smart_kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `nama_kriteria` varchar(45) NOT NULL,
  `bobot_kriteria` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_kriteria`
--

INSERT INTO `smart_kriteria` (`id_kriteria`, `nama_kriteria`, `bobot_kriteria`) VALUES
(18, 'Kehadiran', 0.3),
(19, 'Proses kerja', 0.25),
(20, 'Target yang dicapai', 0.3),
(21, 'Hasil proyek', 0.35);

-- --------------------------------------------------------

--
-- Table structure for table `smart_status_proyek`
--

CREATE TABLE `smart_status_proyek` (
  `id_status_proyek` int(11) NOT NULL,
  `data_proyek_id` int(11) DEFAULT NULL,
  `waktu_tanggal` date DEFAULT NULL,
  `kendala` varchar(255) DEFAULT 'Tidak ada',
  `penyebab_kendala` varchar(255) DEFAULT 'Tidak ada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_status_proyek`
--

INSERT INTO `smart_status_proyek` (`id_status_proyek`, `data_proyek_id`, `waktu_tanggal`, `kendala`, `penyebab_kendala`) VALUES
(10, 11, '2019-11-19', 'tidak ada haha', 'tidak ada haha');

-- --------------------------------------------------------

--
-- Table structure for table `smart_sub_kriteria`
--

CREATE TABLE `smart_sub_kriteria` (
  `id_sub_kriteria` int(11) NOT NULL,
  `nama_sub_kriteria` varchar(45) NOT NULL,
  `nilai_sub_kriteria` double NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smart_sub_kriteria`
--

INSERT INTO `smart_sub_kriteria` (`id_sub_kriteria`, `nama_sub_kriteria`, `nilai_sub_kriteria`, `id_kriteria`, `keterangan`) VALUES
(39, 'Sangat Baik', 100, 18, 'Selalu hadir'),
(40, 'Baik', 75, 18, 'Tidak hadir 2 kali'),
(41, 'Cukup', 50, 18, ''),
(43, 'Sangat Kurang', 25, 18, ''),
(45, 'Sangat Baik', 100, 19, ''),
(46, 'Baik', 75, 19, ''),
(47, 'Cukup', 50, 19, ''),
(49, 'Sangat Kurang', 25, 19, ''),
(50, 'Sangat Baik', 100, 20, ''),
(52, 'Baik', 75, 20, ''),
(54, 'Cukup', 50, 20, ''),
(61, 'Sangat Baik', 100, 21, ''),
(62, 'Baik', 75, 21, ''),
(64, 'Cukup', 50, 21, ''),
(67, 'Sangat Kurang', 25, 21, ''),
(68, 'Sangat Kurang', 25, 20, '');

-- --------------------------------------------------------

--
-- Table structure for table `smart_user`
--

CREATE TABLE `smart_user` (
  `id_user` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `smart_admin`
--
ALTER TABLE `smart_admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `smart_alternatif`
--
ALTER TABLE `smart_alternatif`
  ADD PRIMARY KEY (`id_alternatif`);

--
-- Indexes for table `smart_alternatif_kriteria`
--
ALTER TABLE `smart_alternatif_kriteria`
  ADD PRIMARY KEY (`id_alternatif`,`id_kriteria`);

--
-- Indexes for table `smart_data_proyek`
--
ALTER TABLE `smart_data_proyek`
  ADD PRIMARY KEY (`id_data_proyek`);

--
-- Indexes for table `smart_karyawan`
--
ALTER TABLE `smart_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `smart_karyawan_proyek`
--
ALTER TABLE `smart_karyawan_proyek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smart_kemajuan_proyek`
--
ALTER TABLE `smart_kemajuan_proyek`
  ADD PRIMARY KEY (`id_kemajuan_proyek`);

--
-- Indexes for table `smart_kriteria`
--
ALTER TABLE `smart_kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `smart_status_proyek`
--
ALTER TABLE `smart_status_proyek`
  ADD PRIMARY KEY (`id_status_proyek`);

--
-- Indexes for table `smart_sub_kriteria`
--
ALTER TABLE `smart_sub_kriteria`
  ADD PRIMARY KEY (`id_sub_kriteria`);

--
-- Indexes for table `smart_user`
--
ALTER TABLE `smart_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `smart_admin`
--
ALTER TABLE `smart_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `smart_alternatif`
--
ALTER TABLE `smart_alternatif`
  MODIFY `id_alternatif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `smart_data_proyek`
--
ALTER TABLE `smart_data_proyek`
  MODIFY `id_data_proyek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `smart_karyawan`
--
ALTER TABLE `smart_karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `smart_karyawan_proyek`
--
ALTER TABLE `smart_karyawan_proyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `smart_kemajuan_proyek`
--
ALTER TABLE `smart_kemajuan_proyek`
  MODIFY `id_kemajuan_proyek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `smart_kriteria`
--
ALTER TABLE `smart_kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `smart_status_proyek`
--
ALTER TABLE `smart_status_proyek`
  MODIFY `id_status_proyek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `smart_sub_kriteria`
--
ALTER TABLE `smart_sub_kriteria`
  MODIFY `id_sub_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `smart_user`
--
ALTER TABLE `smart_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
