<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Status Data Proyek</h3>
	</div>
<?php
if($page=='form'){
?>
	<div class="cell colspan2 align-right">
		<a href="status-proyek.php" class="button info">Kembali</a>
	</div>
</div>
	<p></p>
	<?php
	if(isset($_POST['simpan'])){

		$id_data_proyek 			= $_POST['proyek'];
		$waktu_tanggal 				= $_POST['waktu_tanggal'];
		$kendala 					= $_POST['kendala'];
		$penyebab 			        = $_POST['penyebab'];
		
		$stmt 						= $db->prepare("insert into smart_status_proyek values('',?,?,?,?)");
		$stmt->bindParam(1,$id_data_proyek);
		$stmt->bindParam(2,$waktu_tanggal);
		$stmt->bindParam(3,$kendala);
		$stmt->bindParam(4,$penyebab);
		if($stmt->execute()){
			?>
			<script type="text/javascript">location.href='status-proyek.php'</script>
			<?php
		} else{
			?>
			<script type="text/javascript">alert('<?= $kendala ?>')</script>
			<?php
		}
				
	}
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->
				<?php

				$id_status_proyek 	    	= $_GET['id'];			
				$id_data_proyek 			= $_POST['proyek'];
				$waktu_tanggal 				= $_POST['waktu_tanggal'];
				$kendala 					= $_POST['kendala'];
				$penyebab 			        = $_POST['penyebab'];
			
				$stmt = $db->prepare("update smart_status_proyek set data_proyek_id=?, kendala=?, penyebab_kendala=?, waktu_tanggal=? where id_status_proyek=?");

				$stmt->bindParam(1,$id_data_proyek);
				$stmt->bindParam(2,$kendala);
				$stmt->bindParam(3,$penyebab);
				$stmt->bindParam(4,$waktu_tanggal);
				$stmt->bindParam(5,$id_status_proyek);
				
				if($stmt->execute()){ ?>
					<script type="text/javascript">location.href='status-proyek.php'</script>
					<?php
				} else{
					?>
					<script type="text/javascript">alert("Gagal Menyimpan data")</script>
					<?php
				}
		}
	?>
	<!-- Load File javascript config.js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/config.js" type="text/javascript"></script>
	<form method="post">
		<input type="hidden" name="id_kemajuan_proyek" value="<?php echo isset($_GET['id'])? $_GET['id'] : ''; ?>">
        <?php
            //Get all country data
            $query = $db->prepare("select * from smart_data_proyek ORDER BY nama_proyek ASC");
            
            //Count total number of rows
            $query->execute();

            $rowCount = $query->fetch();
        ?>
		<label>Nama Proyek</label>
		<div class="input-control text full-size">
			<select name="proyek" id="proyek">
            <option value="">Plih Proyek</option>
            <?php
            if($rowCount > 0){
                    echo '<option value="'.$rowCount['id_data_proyek'].'">'.$rowCount['nama_proyek'].'</option>';
            }else{
                echo '<option value="">Data proyek tidak tersedia</option>';
            }
            ?>
		    </select>
		</div><br><br>

        <label>Nama Kontraktor</label>
		<div class="input-control text full-size">
		<select name="kontraktor" id="kontraktor">
            <option value="">Pilih kontraktor</option>
        </select>
		</div><br><br>

		<div id="loading" style="margin-top: 15px;">
          <img src="assets/loading/loading.gif" width="18"> <small>Loading...</small>
        </div>

		<label>Tanggal</label>
		<div class="input-control text full-size">
		    <input type="date" name="waktu_tanggal" placeholder="Tanggal" value="<?php echo isset($_GET['tanggal'])? $_GET['tanggal'] : ''; ?>">
		</div>

        <label>Kendala</label>
		<div class="input-control text full-size">
		    <input type="text" name="kendala" placeholder="Kendala" value="<?php echo isset($_GET['kendala'])? $_GET['kendala'] : ''; ?>">
		</div><br><br>

        <label>Penyebab Kendala</label>
		<div class="input-control text full-size">
		    <textarea type="text" name="penyebab" placeholder="Penyebab Kendala"><?php echo isset($_GET['penyebab'])? $_GET['penyebab'] : ''; ?></textarea>
		</div> 
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="simpan" class="button primary">Simpan</button>
			<?php
		}
		?>
	</form>
<?php
} else if($page=='hapus'){
?>
	<div class="cell colspan2 align-right">
	</div>
</div>
<?php
	if(isset($_GET['id'])){
		$stmt = $db->prepare("delete from smart_status_proyek where id_status_proyek ='".$_GET['id']."'");
	 	if($stmt->execute()){
	 		?>
	 		<script type="text/javascript">location.href='status-proyek.php'</script>
	 		<?php
	 	}
	}
} else{
?>
	<div class="cell colspan2 align-right">
		<a href="?page=form" class="button primary">Tambah</a>
	</div>
</div>
<table class="table striped hovered cell-hovered border bordered dataTable" data-role="datatable" data-searching="true">
	<thead>
		<tr>
			<th width="50">ID</th>
			<th width="50">Nama Proyek</th>
			<th width="50">Nama Kontraktor</th>
			<th width="50">Jenis Proyek</th>
			<th width="50">Status</th>
			<th width="50">Gambar</th>
			<th width="100">Kendala</th>
			<th width="100">Penyebab Kendala</th>
			<th width="180">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
        $stmt = $db->prepare("select smart_data_proyek.*, smart_status_proyek.* 
                              from 
                              smart_status_proyek
                              INNER JOIN smart_data_proyek ON smart_data_proyek.id_data_proyek = 
                              smart_status_proyek.data_proyek_id");
		$stmt->execute();
        $no = 1;
		while($row = $stmt->fetch()){
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $row['nama_proyek'] ?></td>
			<td><?php echo $row['nama_kontraktor'] ?></td>
			<td><?php echo $row['jenis_proyek'] ?></td>
			<td><?php echo $row['status'] ?></td>
			<td><img src="assets/gambar/<?= $row['gambar'] ?>" height="100px" width="100px;"></td>
			<td><?php echo $row['kendala'] ?></td>
			<td><?php echo $row['penyebab_kendala'] ?></td>
			<td class="align-center">
				<a href="?page=form&id=<?php echo $row['id_status_proyek'] ?>&id_data_proyek=<?php echo $row['data_proyek_id'] ?>&kendala=<?php echo $row['kendala'] ?>&penyebab=<?php echo $row['penyebab_kendala'] ?>&tanggal=<?php echo $row['waktu_tanggal'] ?>"><span class="mif-pencil icon"></span> Edit</a>
				 | <a href="?page=hapus&id=<?php echo $row['id_status_proyek'] ?>"><span class="mif-cancel icon"></span> Hapus</a>
			</td>
		</tr>
		<?php
		}
		?>penyebab
	</tbody>
</table>
<p><br/></p>
<?php
}
include "footer.php";
?>
					
					