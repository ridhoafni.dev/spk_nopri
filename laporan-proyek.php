<?php
include "config.php";
session_start();
if(!isset($_SESSION['username'])){
	?>
	<script>window.location.assign("login.php")</script>
	<?php
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
</head>

<?php
// Tentukan path yang tepat ke mPDF
$nama_dokumen='Laporan proyek'; //Beri nama file PDF hasil.
require_once __DIR__ . '/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']) // Membuat file mpdf baru
 
//Memulai proses untuk menyimpan variabel php dan html
?>

<div class="container">
	<table align="center">
		<tr>
			<th style="padding-right:2%;"><img src="assets/logo.jpeg" height="50px" width="50px;"/></th>
			<th><h4 style="text-align:center;">Laporan Data Proyek PT. Bukit Mas Jaya Sentosa</h2>
				<p><strong>Jl. KH. Ruddin Nasution Pekanbaru</strong></p>
			</th>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<table>
	<thead>
		<tr style="background:#ededed; padding:10%;">
			<th>No</th>
			<th>Nama Proyek</th>
			<th>Nama Kontraktor</th>
			<th>Jenis Proyek</th>
			<th>Alamat</th>
			<th>Pengawas Lapangan</th>
			<th>Waktu Awal</th>
			<th>Waktu Akhir</th>
			<th>Jumlah Pekerja</th>
		</tr> 
	</thead>
	<tbody>
		<?php
		$stmt = $db->prepare("select * from smart_data_proyek");
		$nox = 1;
		$stmt->execute();
		while($row = $stmt->fetch()){
		?>
		<tr>
			<td><?php echo $nox++ ?></td>
			<td><?php echo $row['nama_proyek'] ?></td>
			<td><?php echo $row['nama_kontraktor'] ?></td>
			<td><?php echo $row['jenis_proyek'] ?></td>
			<td><?php echo $row['alamat'] ?></td>
			<td><?php echo $row['pengawas_lapangan'] ?></td>
			<td><?php echo $row['waktu_pelaksanaan_awal'] ?></td>
			<td><?php echo $row['waktu_pelaksanaan_akhir'] ?></td>
			<td><?php echo $row['jumlah_pekerja'] ?></td>
		<?php
		}
		?>
	</tbody>
	</table>

	<br>
	<p style="margin-left:910px; font-size:10px;font-family:'Times New Roman', Times, serif;">
		Pekanbaru, 2020 
	</p>
	<br>
	<p style="margin-left:910px; font-size:10px;font-family:'Times New Roman', Times, serif;">
		(R. Afrizon, SH. MH) 
	</p>

    <?php
        //penulisan output selesai, sekarang menutup mpdf dan generate kedalam format pdf
        $html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
        ob_end_clean();
        //Disini dimulai proses convert UTF-8, kalau ingin ISO-8859-1 cukup dengan mengganti $mpdf->WriteHTML($html);
        $stylesheet = file_get_contents('assets/style.css');
        $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->Output($nama_dokumen.".pdf" ,'I');
        exit;
    ?>

</html>