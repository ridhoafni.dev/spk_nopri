<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Data Karyawan Proyek</h3>
	</div>
<?php
if($page=='form'){
?>
	<div class="cell colspan2 align-right">
		<a href="karyawan-proyek.php" class="button info">Kembali</a>
	</div>
</div>
	<p></p>
	<?php
	if(isset($_POST['simpan'])){

		//$id_data_proyek 			= $_POST['proyek'];
		$proyek 					= $_POST['proyek'];
		$karyawan 					= $_POST['karyawan'];
		print_r($karyawan);
		foreach($_POST['karyawan'] as $option_num => $option_val){
			echo $option_num." ".$option_val."<br>";
			$stmt = $db->prepare("insert into smart_karyawan_proyek values('',?,?)");
			$stmt->bindParam(1,$proyek);
			$stmt->bindParam(2,$option_val);
			if($stmt->execute()){
				?>
			<script type="text/javascript">location.href='karyawan-proyek.php'</script>
				<?php
			} else{
				?>
			<script type="text/javascript">alert('Gagal menyimpan data')</script>
			<?php
			}
		}
		//$user_id 					= $_SESSION['username'];
				
	}
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->
				<?php
				
				$id_kemajuan_proyek 	    = $_GET['id'];				
				$id_data_proyek 			= $_POST['proyek'];
				$kemajuan 					= $_POST['kemajuan'];
				$tanggal 					= $_POST['tanggal'];
				$user_id 					= $_SESSION['username'];
			
				$stmt = $db->prepare("update smart_kemajuan_proyek set data_proyek_id=?, kemajuan=?, tanggal=? where id_kemajuan_proyek=?");

				$stmt->bindParam(1,$id_data_proyek);
				$stmt->bindParam(2,$kemajuan);
				$stmt->bindParam(3,$tanggal);
				$stmt->bindParam(4,$id_kemajuan_proyek);
				if($stmt->execute()){
					?>
					<script type="text/javascript">location.href='kemajuan-data-proyek.php'</script>
					<?php
				} else{
					?>
					<script type="text/javascript">alert("Gagal Menyimpan data")</script>
					<?php
				}
		}
	?>
	<!-- Load File javascript config.js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/config.js" type="text/javascript"></script>
	<form method="post">
		<input type="hidden" name="id_kemajuan_proyek" value="<?php echo isset($_GET['id'])? $_GET['id'] : ''; ?>">
        <?php
            //Get all country data
            $query = $db->prepare("select * from smart_data_proyek ORDER BY nama_proyek ASC");
            
            //Count total number of rows
            $query->execute();

            $rowCount = $query->fetch();
        ?>
		<label>Data Karyawan Proyek</label>
		<div class="input-control text full-size">
			<select name="proyek" id="proyek">
		    	<option value="<?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?>"><?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?></option>
		    	<?php
				$stmt3 = $db->prepare("select * from smart_data_proyek");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
		    	<option value="<?php echo $row3['id_data_proyek'] ?>"><?php echo $row3['nama_proyek'] ?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</div>

        <div id="loading" style="margin-top: 15px;">
          <img src="assets/loading/loading.gif" width="18"> <small>Loading...</small>
        </div>

        <label>Jumlah Karyawan</label>
		<div class="input-control text full-size">
		<select name="kontraktor" id="kontraktor">
            <option value="">Jumlah Karyawan</option>
        </select>
		</div>
    
		<table class="table striped hovered cell-hovered border bordered dataTable">
			<thead>
				<tr>
					<th>No</th>
					<th>Karyawan</th>
					<th>Pilih</th>

				</tr>
			</thead>
			<tbody>
				<?php
					$stmt3 = $db->prepare("select * from smart_karyawan");
					$stmt3->execute();
					$data = $stmt3->fetchAll();
				?>
				<?php $no = 1;
					foreach ($data as $value) {
				?>
				<tr>
					<td width="10px;"><?php echo $no ?>. </td>
					<td><?php echo $value['nama_karyawan'] ?></td>
					<td width="10px;">
						<input 
						type="checkbox"
						name="karyawan[<?=$value['id_karyawan']?>]"
						value="<?=$value['id_karyawan']?>"/>
					</td>
				</tr>
				<?php $no++; } 
				?>
			</tbody>
		</table>

		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="simpan" class="button primary">Simpan</button>
			<?php
		}
		?>
	</form>
<?php
} else if($page=='hapus'){
?>
	<div class="cell colspan2 align-right">
	</div>
</div>
<?php
	if(isset($_GET['id'])){
		$stmt = $db->prepare("delete from smart_karyawan_proyek where id ='".$_GET['id']."'");
	 	if($stmt->execute()){
	 		?>
	 		<script type="text/javascript">location.href='karyawan-proyek.php'</script>
	 		<?php
	 	}
	}
} else{
?>
	<div class="cell colspan2 align-right">
		<a href="?page=form" class="button primary">Tambah</a>
	</div>
</div>
<table class="table striped hovered cell-hovered border bordered dataTable" data-role="datatable" data-searching="true">
	<thead>
		<tr>
			<th width="50">No.</th>
			<th>Nama Proyek</th>
			<th>Karyawan</th>
			<th width="240">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
        $stmt = $db->prepare("SELECT
								smart_karyawan.*,
								smart_karyawan_proyek.*,
								smart_data_proyek.*
							FROM
								smart_karyawan_proyek
							INNER JOIN smart_data_proyek ON smart_data_proyek.id_data_proyek = smart_karyawan_proyek.proyek_id
							INNER JOIN smart_karyawan ON smart_karyawan_proyek.karyawan_id = smart_karyawan.id_karyawan"
							);
		$stmt->execute();
        $no = 1;
		while($row = $stmt->fetch()){
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $row['nama_proyek'] ?></td>
			<td><?php echo $row['nama_karyawan'] ?></td>
			<td class="align-center"><a href="?page=hapus&id=<?php echo $row['id'] ?>"><span class="mif-cancel icon"></span> Hapus</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<p><br/></p>
<?php
}
include "footer.php";
?>
				