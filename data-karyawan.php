<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Data Karyawan</h3>
	</div>
<?php
if($page=='form'){
?>
	<div class="cell colspan2 align-right">
		<a href="data-karyawan.php" class="button info">Kembali</a>
	</div>
</div>
	<p></p>
	<?php
	if(isset($_POST['simpan'])){

				$nama_karyawan 				= $_POST['nama_karyawan'];
				$alamat 			        = $_POST['alamat'];
				$jabatan 				    = $_POST['jabatan'];
				$email 					    = $_POST['email'];
				$no_hp 			            = $_POST['no_hp'];
				$bidang_keahlian 	        = $_POST['bidang_keahlian'];

				//upload gambar
				$ekstensi_diperbolehkan 	= array('png', 'jpg');

				$photo 					    = $_FILES['photo']['name'];
				$e							= explode('.', $photo);
				$eksensi 					= strtolower(end($e));
				$ukuran						= $_FILES['photo']['size'];
				$file_tmp					= $_FILES['photo']['tmp_name'];
                
				
				if(in_array($eksensi, $ekstensi_diperbolehkan) === true){
					if($ukuran < 1044070){
						move_uploaded_file($file_tmp, 'assets/gambar/'.$photo);

						$stmt = $db->prepare("insert into smart_karyawan values('',?,?,?,?,?,?,?)");

						$stmt->bindParam(1,$nama_karyawan);
						$stmt->bindParam(2,$photo);
						$stmt->bindParam(3,$alamat);
						$stmt->bindParam(4,$jabatan);
						$stmt->bindParam(5,$email);
						$stmt->bindParam(6,$no_hp);
						$stmt->bindParam(7,$bidang_keahlian);
		
						if($stmt->execute()){
							?>
							<script type="text/javascript">location.href='data-karyawan.php'</script>
							<?php
						} else{
							?>
							<script type="text/javascript">alert('Gagal menyimpan data')</script>
							<?php
						}
					}
				}			
	}
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->

				
				<?php  
				$id_karyawan 			    = $_GET['id'];
				$nama_karyawan 				= $_POST['nama_karyawan'];
				$alamat 			        = $_POST['alamat'];
				$jabatan 				    = $_POST['jabatan'];
				$email 					    = $_POST['email'];
				$no_hp 			            = $_POST['no_hp'];
				$bidang_keahlian 	        = $_POST['bidang_keahlian'];

				//upload gambar
				$ekstensi_diperbolehkan 	= array('png', 'jpg');

				$photo 					    = $_FILES['photo']['name'];
				$e							= explode('.', $photo);
				$eksensi 					= strtolower(end($e));
				$ukuran						= $_FILES['photo']['size'];
				$file_tmp					= $_FILES['photo']['tmp_name'];

				if($photo != null){

					if(in_array($eksensi, $ekstensi_diperbolehkan) === true){
						if($ukuran < 1044070){
							move_uploaded_file($file_tmp, 'assets/gambar/'.$photo);
	
							$stmt = $db->prepare("update smart_karyawan set nama_karyawan=?, photo=?, alamat=?, jabatan=?, email=?, no_hp=?, bidang_keahlian=? where id_karyawan=?");
	
							$stmt->bindParam(1,$nama_karyawan);
							$stmt->bindParam(2,$photo);
							$stmt->bindParam(3,$alamat);
							$stmt->bindParam(4,$jabatan);
							$stmt->bindParam(5,$email);
							$stmt->bindParam(6,$no_hp);
							$stmt->bindParam(7,$bidang_keahlian);
							$stmt->bindParam(8,$id_karyawan);
			
							if($stmt->execute()){
								?>
								<script type="text/javascript">location.href='data-karyawan.php'</script>
								<?php
							} else{
								?>
								<script type="text/javascript">alert('Gagal menyimpan data')</script>
								<?php
							}
						}
					}

				}else{

					$stmt = $db->prepare("update smart_karyawan set nama_karyawan=?, alamat=?, jabatan=?, email=?, no_hp=?, bidang_keahlian=? where id_karyawan=?");

					$stmt->bindParam(1,$nama_karyawan);
					$stmt->bindParam(2,$alamat);
					$stmt->bindParam(3,$jabatan);
					$stmt->bindParam(4,$email);
					$stmt->bindParam(5,$no_hp);
					$stmt->bindParam(6,$bidang_keahlian);
					$stmt->bindParam(7,$id_karyawan);
	
					if($stmt->execute()){ ?>
						<script type="text/javascript">location.href='data-karyawan.php'</script>
						<?php
					} else{
						?>
						<script type="text/javascript">alert("<?= $id_karyawan ?>")</script>
						<?php
					}

				}
		}
	?>
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="id_data_proyek" value="<?php echo isset($_GET['id'])? $_GET['id_data_proyek'] : ''; ?>">
		<label>Photo</label>
		<div class="input-control text full-size">
		    <input type="file" name="photo" placeholder="photo">
		</div><br><br>
        <label>Nama Karyawan</label>
		<div class="input-control text full-size">
		    <input type="text" name="nama_karyawan" placeholder="Nama Karyawan" value="<?php echo isset($_GET['nama_karyawan'])? $_GET['nama_karyawan'] : ''; ?>">
		</div><br><br>
		
        <label>Alamat</label>
		<div class="input-control text full-size">
		    <textarea type="text" name="alamat" placeholder="Alamat Lengkap"><?php echo isset($_GET['alamat'])? $_GET['alamat'] : ''; ?></textarea>
		</div><br><br><br><br><br><br>
        
        <label>Jabatan</label>
		<div class="input-control text full-size">
		    <input type="text" name="jabatan" placeholder="Jabatan" value="<?php echo isset($_GET['jabatan'])? $_GET['jabatan'] : ''; ?>">
		</div><br><br>

        <label>Email</label>
		<div class="input-control text full-size">
		    <input type="email" name="email" placeholder="Email" value="<?php echo isset($_GET['email'])? $_GET['email'] : ''; ?>">
		</div><br><br>

        <label>Nomor HP</label>
		<div class="input-control text full-size">
		    <input type="text" name="no_hp" placeholder="Nomor HP" value="<?php echo isset($_GET['no_hp'])? $_GET['no_hp'] : ''; ?>">
		</div><br><br>
		
        <label>Bidang Keahlian</label>
		<div class="input-control text full-size">
			<select name="bidang_keahlian">
		    	<!-- <option value="<?php //echo isset($_GET['jenis_proyek'])? $_GET['jenis_proyek'] : ''; ?>"><?php //echo isset($_GET['jenis_proyek'])? $_GET['jenis_proyek'] : ''; ?></option> -->
		    	<option value="Semenisasi" <?php if(isset($_GET['bidang_keahlian']) AND $_GET['bidang_keahlian'] == "Semenisasi"){ echo "selected"; } ?> >Semenisasi</option>
		    	<option value="Pemasangan Pintu" <?php if(isset($_GET['bidang_keahlian']) AND $_GET['bidang_keahlian'] == "Pemasangan Pintu"){ echo "selected"; } ?> >Pemasangan Pintu</option>
		    	<option value="Pengecatan" <?php if(isset($_GET['bidang_keahlian']) AND $_GET['bidang_keahlian'] == "Pengecatan"){ echo "selected"; } ?> >Pengecatan</option>
		    	<option value="Dll..." <?php if(isset($_GET['bidang_keahlian']) AND $_GET['bidang_keahlian'] == "Dll..."){ echo "selected"; } ?> >Dll...</option>
		    </select>
		</div><br><br>

		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="simpan" class="button primary">Simpan</button>
			<?php
		}
		?>
	</form>
<?php
} else if($page=='hapus'){
?>
	<div class="cell colspan2 align-right">
	</div>
</div>
<?php
	if(isset($_GET['id'])){
		$stmt = $db->prepare("delete from smart_karyawan where id_karyawan ='".$_GET['id']."'");
	 	if($stmt->execute()){
	 		?>
	 		<script type="text/javascript">location.href='data-karyawan.php'</script>
	 		<?php
	 	}
	}
} else{
?>
	<div class="cell colspan2 align-right">
		<a href="?page=form" class="button primary">Tambah</a>
	</div>
</div>
<table class="table striped hovered cell-hovered border bordered dataTable" data-role="datatable" data-searching="true">
	<thead>
		<tr>
			<th width="50">ID</th>
			<th width="50">Nama Karyawan</th>
			<th width="50">Photo</th>
			<th width="50">Email</th>
			<th width="50">Jabatan</th>
			<th width="50">No HP</th>
			<th width="50">Bidang Keahlian</th>
			<th width="50">Alamat</th>
			<th width="240">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$stmt = $db->prepare("select * from smart_karyawan");
		$stmt->execute();
        $no = 1;
		while($row = $stmt->fetch()){
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $row['nama_karyawan'] ?></td>
			<td><img src="assets/gambar/<?=$row['photo'] ?>" height="100px" width="100px;"></td>
			<td><?php echo $row['email'] ?></td>
			<td><?php echo $row['jabatan'] ?></td>
			<td><?php echo $row['no_hp'] ?></td>
			<td><?php echo $row['bidang_keahlian'] ?></td>
			<td><?php echo $row['alamat'] ?></td>
			<td class="align-center">
				<a href="?page=form&id=<?php echo $row['id_karyawan'] ?>&nama_karyawan=<?php echo $row['nama_karyawan'] ?>&email=<?php echo $row['email'] ?>&jabatan=<?php echo $row['jabatan'] ?>&alamat=<?php echo $row['alamat'] ?>&bidang_keahlian=<?php echo $row['bidang_keahlian'] ?>&no_hp=<?php echo $row['no_hp'] ?>"><span class="mif-pencil icon"></span> Edit</a>
				 | <a href="?page=hapus&id=<?php echo $row['id_karyawan'] ?>"><span class="mif-cancel icon"></span> Hapus</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<p><br/></p>
<?php
}
include "footer.php";
?>
					
					