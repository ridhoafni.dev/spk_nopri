<?php

include "config.php";
session_start();
if(!isset($_SESSION['username'])){
	?>
	<script>window.location.assign("login.php")</script>
	<?php
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>PENILAIAN KARYAWAN</title>
    <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
</head>
<body  bgcolor="#C0C0C0">
    <div class="app-bar" style="background-color: #004d40">
		<a class="app-bar-element" href="...">PENILAIAN KARYAWAN</a>
		<span class="app-bar-divider"></span>
		<!-- <ul class="app-bar-menu">
			<li><a href="kriteria.php">Kriteria</a></li>
			<li><a href="subkriteria.php">Sub Kriteria</a></li>
			<li><a href="alternatif.php">Alternatif</a></li>
			<li><a href="perangkingan.php">Perangkingan</a></li>
			<li><a target="_blank" href="laporan.php">Laporan</a></li>
			<li>
				<a href="" class="dropdown-toggle">Laporan</a>
				<ul class="d-menu" data-role="dropdown">
					<li><a href="">Direct</a></li>
					<li><a href="">FPDF</a></li>
					<li><a href="">phpToPDF</a></li>
					<li><a href="">TCPDF</a></li>
					<li><a href="">Dompdf</a></li>
					<li><a href="">Zend_Pdf</a></li>
					<li><a href="">PDFlib</a></li>
					<li><a href="">mPDF</a></li>
				</ul>
			</li>
		</ul> -->
		<a href="logout.php" class="app-bar-element place-right">Logout</a>
	</div>
	
	<div style="padding:5px 20px; ">
		<div class="grid">
			<div class="row cells5">
            <?php
            include "main.php";
            ?>
				<div class="cell colspan4" style="background:#C0C0C0">
				<center><h3><i>

</ol>
<br><br>
<div style="padding:10px 15px;border:1px solid blue;background:white;">
<script src="js/Chart.js"></script>

<canvas id="myChart" style="width:100%;height:400px;"></canvas>
<script>
var ctx = document.getElementById("myChart").getContext("2d");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            <?php
            $stmt2x = $db->prepare("select * from smart_alternatif");
            $stmt2x->execute();
            while($row2x = $stmt2x->fetch()){
            ?>
            "<?php echo $row2x['nama_alternatif'] ?>",
            <?php
            }
            ?>
        ],
        datasets: [{
            label: '# of Votes',
            data: [
            <?php
            $stmt2y = $db->prepare("select * from smart_alternatif");
            $stmt2y->execute();
            while($row2y = $stmt2y->fetch()){
                echo $row2y['hasil_alternatif'].',';
            }
            ?>
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',
                'rgba(75, 192, 192, 0.8)',
                'rgba(153, 102, 255, 0.8)',
                'rgba(245, 159, 64, 0.8)',
                'rgba(223, 99, 132, 0.8)',
                'rgba(45, 162, 235, 0.8)',
                'rgba(211, 206, 86, 0.8)',
                'rgba(89, 192, 192, 0.8)',
                'rgba(233, 99, 132, 0.8)',
                'rgba(67, 167, 235, 0.8)',
                'rgba(20, 26, 86, 0.8)',
                'rgba(67, 42, 12, 0.8)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        },
        title: {
            display: true,
            text: 'Hasil Akhir Perangkingan'
        }
    }
});
</script>
<?php
include "footer.php";
?>
					
					