<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Data Jenis Proyek</h3>
	</div>
<?php
if($page=='form'){
?>
	<div class="cell colspan2 align-right">
		<a href="jenis-proyek.php" class="button info">Kembali</a>
	</div>
</div>
	<p></p>
	<?php
	if(isset($_POST['simpan'])){

				$nama_proyek 				= $_POST['nama_proyek'];
				$nama_kontraktor 			= $_POST['nama_kontraktor'];
				$jenis_proyek 				= $_POST['jenis_proyek'];
				$alamat 					= $_POST['alamat'];
				$pengawas_lapangan 			= $_POST['pengawas_lapangan'];
				$waktu_pelaksanaan_awal 	= $_POST['waktu_pelaksanaan_awal'];
				$waktu_pelaksanaan_akhir 	= $_POST['waktu_pelaksanaan_akhir'];
				$jumlah_pekerja 			= $_POST['jumlah_pekerja'];

				//upload gambar
				$ekstensi_diperbolehkan 	= array('png', 'jpg');

				$gambar 					= $_FILES['gambar']['name'];
				$e							= explode('.', $gambar);
				$eksensi 					= strtolower(end($e));
				$ukuran						= $_FILES['gambar']['size'];
				$file_tmp					= $_FILES['gambar']['tmp_name'];

				$status 					= "Sedang Jalan";
				if(in_array($eksensi, $ekstensi_diperbolehkan) === true){
					if($ukuran < 1044070){
						move_uploaded_file($file_tmp, 'assets/gambar/'.$gambar);
						$stmt 				= $db->prepare("insert into smart_data_proyek values('',?,?,?,?,?,?,?,?,?,?)");
						$stmt->bindParam(1,$nama_proyek);
						$stmt->bindParam(2,$nama_kontraktor);
						$stmt->bindParam(3,$jenis_proyek);
						$stmt->bindParam(4,$alamat);
						$stmt->bindParam(5,$pengawas_lapangan);
						$stmt->bindParam(6,$waktu_pelaksanaan_awal);
						$stmt->bindParam(7,$waktu_pelaksanaan_akhir);
						$stmt->bindParam(8,$jumlah_pekerja);
						$stmt->bindParam(9,$gambar);
						$stmt->bindParam(10,$status);
						if($stmt->execute()){
							?>
							<script type="text/javascript">location.href='data-proyek.php'</script>
							<?php
						} else{
							?>
							<script type="text/javascript">alert('Gagal menyimpan data')</script>
							<?php
						}
					}
				}
				
	}
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->
				<?php  
				$id_data_proyek 			= $_GET['id'];
				$nama_proyek 				= $_POST['nama_proyek'];
				$nama_kontraktor 			= $_POST['nama_kontraktor'];
				$jenis_proyek 				= $_POST['jenis_proyek'];
				$alamat 					= $_POST['alamat'];
				$pengawas_lapangan 			= $_POST['pengawas_lapangan'];
				$waktu_pelaksanaan_awal 	= $_POST['waktu_pelaksanaan_awal'];
				$waktu_pelaksanaan_akhir 	= $_POST['waktu_pelaksanaan_akhir'];
				$jumlah_pekerja 			= $_POST['jumlah_pekerja'];

				//upload gambar
				$ekstensi_diperbolehkan 	= array('png', 'jpg');
				$gambar 					= $_FILES['gambar']['name'];
				$e							= explode('.', $gambar);
				$eksensi 					= strtolower(end($e));
				$ukuran						= $_FILES['gambar']['size'];
				$file_tmp					= $_FILES['gambar']['tmp_name'];
				$status 					= "Sedang Jalan";

				if($gambar != null){
			
					if(in_array($eksensi, $ekstensi_diperbolehkan) === true){
						if($ukuran < 1044070){
							move_uploaded_file($file_tmp, 'assets/gambar/'.$gambar);
							$stmt = $db->prepare("update smart_data_proyek set nama_proyek=?, nama_kontraktor=?, jenis_proyek=?, alamat=?, pengawas_lapangan=?, waktu_pelaksanaan_awal=?, waktu_pelaksanaan_akhir=?, jumlah_pekerja=?, gambar=?, status=? where id_data_proyek=?");
	
							$stmt->bindParam(1,$nama_proyek);
							$stmt->bindParam(2,$nama_kontraktor);
							$stmt->bindParam(3,$jenis_proyek);
							$stmt->bindParam(4,$alamat);
							$stmt->bindParam(5,$pengawas_lapangan);
							$stmt->bindParam(6,$waktu_pelaksanaan_awal);
							$stmt->bindParam(7,$waktu_pelaksanaan_akhir);
							$stmt->bindParam(8,$jumlah_pekerja);
							$stmt->bindParam(9,$gambar);
							$stmt->bindParam(10,$status);
							$stmt->bindParam(11,$id_data_proyek);
							if($stmt->execute()){
								?>
								<script type="text/javascript">location.href='data-proyek.php'</script>
								<?php
							} else{
								?>
								<script type="text/javascript">alert('Gagal menyimpan data')</script>
								<?php
							}
						}
					}
				}else{?>
					<?php
					$stmt = $db->prepare("update smart_data_proyek set nama_proyek=?, nama_kontraktor=?, jenis_proyek=?, alamat=?, pengawas_lapangan=?, waktu_pelaksanaan_awal=?, waktu_pelaksanaan_akhir=?, jumlah_pekerja=?, status=? where id_data_proyek=?");
	
							$stmt->bindParam(1,$nama_proyek);
							$stmt->bindParam(2,$nama_kontraktor);
							$stmt->bindParam(3,$jenis_proyek);
							$stmt->bindParam(4,$alamat);
							$stmt->bindParam(5,$pengawas_lapangan);
							$stmt->bindParam(6,$waktu_pelaksanaan_awal);
							$stmt->bindParam(7,$waktu_pelaksanaan_akhir);
							$stmt->bindParam(8,$jumlah_pekerja);
							$stmt->bindParam(9,$status);
							$stmt->bindParam(10,$id_data_proyek);
							if($stmt->execute()){
								?>
								<script type="text/javascript">location.href='data-proyek.php'</script>
								<?php
							} else{
								?>
								<script type="text/javascript">alert('Gagal update data')</script>
								<?php
							}
				}
		}
	?>
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="id_data_proyek" value="<?php echo isset($_GET['id'])? $_GET['id_data_proyek'] : ''; ?>">
		<label>Nama Proyek</label>
		<div class="input-control text full-size">
		    <input type="text" name="nama_proyek" placeholder="Nama Proyek" value="<?php echo isset($_GET['nama_proyek'])? $_GET['nama_proyek'] : ''; ?>">
		</div>
		<label>Nama Kontraktor</label>
		<div class="input-control text full-size">
		    <input type="text" name="nama_kontraktor" placeholder="Nama Kontraktor" value="<?php echo isset($_GET['nama_kontraktor'])? $_GET['nama_kontraktor'] : ''; ?>">
		</div>
		<label>Jenis Proyek</label>
		<div class="input-control text full-size">
			<select name="jenis_proyek">
		    	<!-- <option value="<?php //echo isset($_GET['jenis_proyek'])? $_GET['jenis_proyek'] : ''; ?>"><?php //echo isset($_GET['jenis_proyek'])? $_GET['jenis_proyek'] : ''; ?></option> -->
		    	<option value="Pembangunan Gedung" <?php if(isset($_GET['jenis_proyek']) AND $_GET['jenis_proyek'] == "Pembangunan Gedung"){ echo "selected"; } ?> >Pembangunan Gedung</option>
		    	<option value="Perumahan" <?php if(isset($_GET['jenis_proyek']) AND $_GET['jenis_proyek'] == "Perumahan"){ echo "selected"; } ?> >Perumahan</option>
		    	<option value="Jalan" <?php if(isset($_GET['jenis_proyek']) AND $_GET['jenis_proyek'] == "Jalan"){ echo "selected"; } ?> >Jalan</option>
		    </select>
		</div>
        <label>Alamat</label>
		<div class="input-control text full-size">
		    <input type="text" name="alamat" placeholder="Alamat" value="<?php echo isset($_GET['alamat'])? $_GET['alamat'] : ''; ?>">
		</div>
        <label>Pengawas Lapangan</label>
		<div class="input-control text full-size">
		    <input type="text" name="pengawas_lapangan" placeholder="Pengawas Lapangan" value="<?php echo isset($_GET['pengawas_lapangan'])? $_GET['pengawas_lapangan'] : ''; ?>">
		</div>
        <label>Waktu Pelaksanaan(Awal)</label>
		<div class="input-control text full-size">
		    <input type="date" name="waktu_pelaksanaan_awal" placeholder="Waktu Pelaksanaan (Awal)" value="<?php echo isset($_GET['waktu_pelaksanaan_awal'])? $_GET['waktu_pelaksanaan_awal'] : ''; ?>">
		</div>
		<label>Waktu Pelaksanaan(Akhir)</label>
		<div class="input-control text full-size">
		    <input type="date" name="waktu_pelaksanaan_akhir" placeholder="Waktu Pelaksanaan (Akhir)" value="<?php echo isset($_GET['waktu_pelaksanaan_akhir'])? $_GET['waktu_pelaksanaan_akhir'] : ''; ?>">
		</div>
        <label>Jumlah Pekerja</label>
		<div class="input-control text full-size">
		    <input type="number" name="jumlah_pekerja" placeholder="Jumlah Pekerja" value="<?php echo isset($_GET['jumlah_pekerja'])? $_GET['jumlah_pekerja'] : ''; ?>">
		</div>
		<label>Status</label>
		<div class="input-control text full-size">
			<select name="status">
		    	<!-- <option value="<?php // echo isset($_GET['status'])? $_GET['status'] : ''; ?>"><?php //echo isset($_GET['status'])? $_GET['status'] : ''; ?></option> -->
		    	<option value="Sedang Jalan" <?php if(isset($_GET['status']) AND $_GET['status'] == "Sedang Jalan"){ echo "selected"; } ?> >Sedang Jalan</option>
		    	<option value="Selesai" <?php if(isset($_GET['status']) AND $_GET['status'] == "Selesai"){ echo "selected"; } ?>>Selesai</option>
		    	<option value="Tidak Selesai" <?php if(isset($_GET['status']) AND $_GET['status'] == "Tidak Selesai"){ echo "selected"; } ?>>Tidak Selesai</option>
		    	<option value="Target Tidak Tepat Waktu" <?php if(isset($_GET['status']) AND $_GET['status'] == "Target Tidak Tepat Waktu"){ echo "selected"; } ?>>Target Tidak Tepat Waktu</option>
		    </select>
		</div>
        <label>Gambar</label>
		<div class="input-control text full-size">
		    <input type="file" name="gambar" placeholder="Gambar">
		</div>
		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="simpan" class="button primary">Simpan</button>
			<?php
		}
		?>
	</form>
<?php
} else if($page=='hapus'){
?>
	<div class="cell colspan2 align-right">
	</div>
</div>
<?php
	if(isset($_GET['id'])){
		$stmt = $db->prepare("delete from smart_data_proyek where id_data_proyek ='".$_GET['id']."'");
	 	if($stmt->execute()){
	 		?>
	 		<script type="text/javascript">location.href='data-proyek.php'</script>
	 		<?php
	 	}
	}
} else{
?>
</div>
<table class="table striped hovered cell-hovered border bordered dataTable" data-role="datatable" data-searching="true">
	<thead>
		<tr>
			<th width="50">ID</th>
			<th width="50">Jenis Proyek</th>
			<th width="50">Nama Proyek</th>
			<th width="50">Nama Kontraktor</th>
			<th width="50">Pengawas Lapangan</th>
			<th width="50">Waktu Mulai (Awal)</th>
			<th width="50">Waktu Selesai (Akhir)</th>
			<th width="50">Status</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$stmt = $db->prepare("select * from smart_data_proyek");
		$stmt->execute();
        $no = 1;
		while($row = $stmt->fetch()){
		?>
		<tr>
			<td><?php echo $no++ ?></td>
            <td> <?php if($row['jenis_proyek'] == "Perumahan") { ?>
                <span class="fg-green"><?php echo $row['jenis_proyek'] ?></span>
            <?php }else{ ?>
                <span class="fg-blue"><?php echo $row['jenis_proyek'] ?></span>
            <?php } ?>
            </td>
			<td><?php echo $row['nama_proyek'] ?></td>
			<td><?php echo $row['nama_kontraktor'] ?></td>
			<td><?php echo $row['pengawas_lapangan'] ?></td>
			<td><?php echo $row['waktu_pelaksanaan_awal'] ?></td>
			<td><?php echo $row['waktu_pelaksanaan_akhir'] ?></td>
			<td><?php echo $row['status'] ?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<p><br/></p>
<?php
}
include "footer.php";
?>
					
					