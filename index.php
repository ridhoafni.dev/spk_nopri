<?php

include "config.php";
session_start();
if(!isset($_SESSION['username'])){
	?>
<script>
window.location.assign("login.php")
</script>
<?php
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>BUKIT MAS JAYA SENTOSA</title>
    <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
</head>
<style type="text/css">
p {
    font-size: 18px;
}

* {
    box-sizing: border-box
}

/* Slideshow container */
.slideshow-container {
    max-width: 1000px;
    position: relative;
    margin: auto;
}

/* Hide the images by default */
.mySlides {
    display: none;
}

/* Next & previous buttons */
.prev,
.next {
    cursor: pointer;
    position: absolute;
    top: 50%;
    width: auto;
    margin-top: -22px;
    padding: 16px;
    color: white;
    font-weight: bold;
    font-size: 18px;
    transition: 0.6s ease;
    border-radius: 0 3px 3px 0;
    user-select: none;
}

/* Position the "next button" to the right */
.next {
    right: 0;
    border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
    background-color: rgba(0, 0, 0, 0.8);
}

/* Caption text */
.text {
    color: #f2f2f2;
    font-size: 15px;
    padding: 8px 12px;
    position: absolute;
    bottom: 8px;
    width: 100%;
    text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
    color: #f2f2f2;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
}

/* The dots/bullets/indicators */
.dot {
    cursor: pointer;
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    transition: background-color 0.6s ease;
}

.active,
.dot:hover {
    background-color: #717171;
}

/* Fading animation */
.fade {
    -webkit-animation-name: fade;
    -webkit-animation-duration: 1.5s;
    animation-name: fade;
    animation-duration: 1.5s;
}

@-webkit-keyframes fade {
    from {
        opacity: .4
    }

    to {
        opacity: 1
    }
}

@keyframes fade {
    from {
        opacity: .4
    }

    to {
        opacity: 1
    }
}
</style>

<body bgcolor="#C0C0C0">
    <div class="app-bar" style="background-color:#004d40;">
        <a class="app-bar-element" href="...">PT BUKIT MAS JAYA SENTOSA</a>
        <span class="app-bar-divider"></span>
        <a href="logout.php" class="app-bar-element place-right">Logout</a>
    </div>

    <div style="padding:2px 10px; ">
        <div class="grid" style="background-color=#004d40">
            <div class="row cells5">
                <?php
            include "main.php";
            ?>
               
                <div class="cell colspan4" style="background:#C0C0C0">
                    <center>
                        <h3><i>
                                </ol>
                                <div style="padding:10px 15px;border:1px solid blue;background:white;">
                                    <script src="js/Chart.js"></script>
                                    <br>

									<!-- Slideshow container -->
					 <div class="slideshow-container">

<!-- Full-width images with number and caption text -->
<div class="mySlides fade">
	<div class="numbertext">1 / 3</div>
	<img src="assets/slide/1.jpg" style="width:100%">
	<div class="text">Caption Text</div>
</div>

<div class="mySlides fade">
	<div class="numbertext">2 / 3</div>
	<img src="assets/slide/2.jpg" style="width:100%">
	<div class="text">Caption Two</div>
</div>

<div class="mySlides fade">
	<div class="numbertext">3 / 3</div>
	<img src="assets/slide/3.jpg" style="width:100%">
	<div class="text">Caption Three</div>
</div>

<!-- Next and previous buttons -->
<!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a> -->
</div>
<br>

<!-- The dots/circles -->
<div style="text-align:center">
<span class="dot" onclick="currentSlide(1)"></span>
<span class="dot" onclick="currentSlide(2)"></span>
<span class="dot" onclick="currentSlide(3)"></span>
</div>

                                    <!-- <img src="assets/logo.jpg" height="100px" width="100px;"/> -->

                                    <p>Visi</p>
                                    <p>Menjadi perusahaan swasta nasional terdepan di industri jasa konstruksi,
                                        berkembang secara berkesinambungan, memberikan kesejahteraan kepada karyawan,
                                        pengurus, pemegang saham dan stake holder lainnya melalui komitmen terhadap CSR
                                        (corporate social responsibility) dan tata kelola perusahaan yang baik (good
                                        corporate governance).</p>
                                    <p>Misi</p>
                                    <p>Adapun misi dari perusahaan adalah :</p>
                                    <p style="text-align:left">
                                        1. Pemberdayaan maksimal dari lima pilar usaha konstruksi: Pemasaran,
                                        Operasional, Keuangan, Sumber Daya Manusia dan Informasi.<br>
                                        2. Menyamakan persepsi diantara manajemen untuk mempertahankan nilai-nilai
                                        perusahaan dan mencapai tujuan bersama.<br>
                                        3. Pelatihan dan rekruitmen sumber daya manusia yang tepat, untuk menghasilkan
                                        tenaga kerja yang kompeten, berdedikasi dan bersemangat tinggi sesuai budaya
                                        perusahaan.<br>
                                        4. Penerapan prinsip kehati-hatian dalam pengambilan keputusan dan tata kelola
                                        perusahaan yang baik (good corporate governance).<br>
                                        5. Peningkatan kompetisi diera globalisasi lewat kerjasama dengan perusahaan
                                        kontraktor nasional maupun internasional.</p>
                                    <?php
include "footer.php";
?>
                                    <script>
                                  var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  slides[slideIndex-1].style.display = "block";
  setTimeout(showSlides, 2000); // Change image every 2 seconds
}
                                    </script>