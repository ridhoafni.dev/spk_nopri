<?php
include "config.php";
session_start();
if(!isset($_SESSION['username'])){
	?>
	<script>window.location.assign("login.php")</script>
	<?php
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
</head>
<body>

<?php
// Tentukan path yang tepat ke mPDF
$nama_dokumen='laporan penilaian kinerja karyawan'; //Beri nama file PDF hasil.
require_once __DIR__ . '/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['orientation' => 'P', 'debug'=>true]) // Membuat file mpdf baru
 
//Memulai proses untuk menyimpan variabel php dan html
?>
	<div class="container">

	<table align="center">
		<tr>
			<th style="padding-right:2%;"><img src="assets/logo.jpeg" height="50px" width="50px;"/></th>
			<th><h4 style="text-align:center;">LAPORAN PERANGKINGAN SISTEM KINERJA KARYAWAN</h2></th>
		</tr>
	</table>
	<br>
	<p><strong>Nilai Dasar</strong></p>

	<table>
		<thead>
			<tr style="background:#ededed; padding:10%;">
				<th>No</th>
				<th>Alternatif</th>
				<?php
				$stmt2 = $db->prepare("select * from smart_kriteria");
				$stmt2->execute();
				while($row2 = $stmt2->fetch()){
				?>
				<th><?php echo $row2['nama_kriteria'] ?></th>
				<?php
				}
				?>
			</tr> 
		</thead>
		<tbody>
			<?php
			$stmt = $db->prepare("select smart_alternatif.*, smart_karyawan.* 
			from smart_alternatif
			INNER JOIN smart_karyawan ON smart_alternatif.nama_alternatif = 
			smart_karyawan.id_karyawan");
			$nox = 1;
			$stmt->execute();
			while($row = $stmt->fetch()){
			?>
			<tr>
				<td><?php echo $nox++ ?></td>
				<td><?php echo $row['nama_karyawan'] ?></td>
				<?php
				$stmt3 = $db->prepare("select * from smart_kriteria");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
				<td>
					<?php
					$stmt4 = $db->prepare("select * from smart_alternatif_kriteria where id_kriteria='".$row3['id_kriteria']."' and id_alternatif='".$row['id_alternatif']."'");
					$stmt4->execute();
					while($row4 = $stmt4->fetch()){
						echo $row4['nilai_alternatif_kriteria'];
						?>
						<!--<a href="?page=form&alt=<?php echo $row['id_alternatif'] ?>&kri=<?php echo $row3['id_kriteria'] ?>&nilai=<?php echo $row4['nilai_alternatif_kriteria'] ?>" style="color:orange"><span class="mif-pencil icon"></span></a>-->
						<?php
					}
					?>
				</td>
				<?php
				}
				?>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>

	<br/>
	<p><strong>Nilai Perangkingan</strong></p>
	
	<table class="table">
		<thead>
			<tr style="background:#ededed; padding:15px 35px; border-left:1px solid #e0e0e0; border-bottom: 1px solid #e0e0e0;">
				<th>No</th>
				<th>Alternatif</th>
				<?php
				$stmt2x = $db->prepare("select * from smart_kriteria");
				$stmt2x->execute();
				while($row2x = $stmt2x->fetch()){
				?>
				<th><?php echo $row2x['nama_kriteria'] ?></th>
				<?php
				}
				?>
				<th>Hasil</th>
				<th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>-</td>
				<td>Bobot</td>
				<?php
				$stmt2x1 = $db->prepare("select * from smart_kriteria");
				$stmt2x1->execute();
				while($row2x1 = $stmt2x1->fetch()){
				?>
				<td><?php echo $row2x1['bobot_kriteria'] ?></td>
				<?php
				}
				?>
				<td>-</td>
				<td>-</td>
			</tr>
			<?php
			$stmtx = $db->prepare("select smart_alternatif.*, smart_karyawan.* 
			from smart_alternatif
			INNER JOIN smart_karyawan ON smart_alternatif.nama_alternatif = 
			smart_karyawan.id_karyawan");
			$noxx = 1;
			$stmtx->execute();
			while($rowx = $stmtx->fetch()){
			?>
			<tr>
				<td><?php echo $noxx++ ?></td>
				<td><?php echo $rowx['nama_karyawan'] ?></td>
				<?php
				$stmt3x = $db->prepare("select * from smart_kriteria");
				$stmt3x->execute();
				while($row3x = $stmt3x->fetch()){
				?>
				<td>
					<?php
					$stmt4x = $db->prepare("select * from smart_alternatif_kriteria where id_kriteria='".$row3x['id_kriteria']."' and id_alternatif='".$rowx['id_alternatif']."'");
					$stmt4x->execute();
					while($row4x = $stmt4x->fetch()){
						$ida = $row4x['id_alternatif'];
						$idk = $row4x['id_kriteria'];
						echo $kal = $row4x['nilai_alternatif_kriteria']*$row3x['bobot_kriteria'];
						$stmt2x3 = $db->prepare("update smart_alternatif_kriteria set bobot_alternatif_kriteria=? where id_alternatif=? and id_kriteria=?");
						$stmt2x3->bindParam(1,$kal);
						$stmt2x3->bindParam(2,$ida);
						$stmt2x3->bindParam(3,$idk);
						$stmt2x3->execute();
					}
					?>
				</td>
				<?php
				}
				?>
				<td>
					<?php
					$stmt3x2 = $db->prepare("select sum(bobot_alternatif_kriteria) as bak from smart_alternatif_kriteria where id_alternatif='".$rowx['id_alternatif']."'");
					$stmt3x2->execute();
					$row3x2 = $stmt3x2->fetch();
					$ideas = $rowx['id_alternatif'];
					echo $hsl = $row3x2['bak'];
					if($hsl>=80){
						$ket = "Sangat Layak";
					} else if($hsl>=60){
						$ket = "Layak";
					} else if($hsl>=40){
						$ket = "Dipertimbangkan";
					} else{
						$ket = "Tidak Layak";
					}
					?>
				</td>
				<td>
					<?php
					if($hsl>=80){
						$ket2 = "Sangat Layak";
					} else if($hsl>=55){
						$ket2 = "Layak";
					} else if($hsl>=35){
						$ket2 = "Dipertimbangkan";
					} else{
						$ket2 = "Tidak Layak";
					}
					echo $ket2;
					?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>

</div>
<?php 
 //penulisan output selesai, sekarang menutup mpdf dan generate kedalam format pdf
 $html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
 ob_end_clean();
 //Disini dimulai proses convert UTF-8, kalau ingin ISO-8859-1 cukup dengan mengganti $mpdf->WriteHTML($html);
 $stylesheet = file_get_contents('assets/style.css');
 $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
 $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
 $mpdf->Output($nama_dokumen.".pdf" ,'I');
 exit;
 ?>
</body>
</html>