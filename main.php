<div class="cell">
    <ul class="v-menu" style="border:1px solid blue; background:#004d40">
        <li class="menu-title" style="background:#004d40"> <font color="white"><b>Dashboard </b></font></li>
        <li><a href="index.php"><span class="mif-home icon"></span>  <font color="white"><b>Beranda</a></b></font></li>
        <?php 
        include "config.php";
        $id_user_now = $_SESSION['id'];
        $stmt = $db->prepare("SELECT * from smart_admin where id_admin='$id_user_now'");
        $stmt->execute();
        $row = $stmt->fetch();
        if($row['level'] == "pimpinan"){ ?>
                <li><a href="laporan.php" target="_blank"><span class="mif-bookmark icon"></span>  <font color="white"><b>Laporan Penilain</b></font></a></li>
                <li><a href="laporan-proyek.php" target="_blank"><span class="mif-chart-line icon"></span>  <font color="white"><b>Laporan Proyek</b></font></a></li>
                <li><a href="status-proyek.php"><span class="mif-cabinet icon"></span>  <font color="white"><b>Status Proyek</b></font></a></li>
        <?php 
            }elseif($row['level'] == "koordinator lapangan"){ 
        ?>
        <li class="menu-title" style="background:#004d40"> <font color="white"><b>Penilaian</b></font></li>
        <li  style="background:#004d40">
            <a href="#"><span class="mif-layers icon"></span> <font color="white"><b>Penilaian Kinerja</b></font></a>
            <ul class="v-menu" data-role="dropdown">
                <!-- <li><a href="kriteria.php">Kriteria</a></li>
                <li><a href="subkriteria.php">Sub Kriteria</a></li>
                <li><a href="alternatif.php">Alternatif</a></li> -->
                <li><a href="perangkingan.php">Perangkingan</a></li>
                <li><a href="cari-karyawan-proyek.php">Cari Perangkingan</a></li>
                <li><a href="grafik-penilaian.php" target="_blank">Grafik Penilaian</a></li>
                <li><a href="laporan.php" target="_blank">Laporan Penilaian</a></li>
            </ul>
        </li>
        <li><a href="kemajuan-data-proyek.php"><span class="mif-flag icon"></span>  <font color="white"><b>Kemajuan Proyek</b></font></a></li>
        <li><a href="jenis-proyek.php"><span class="mif-books icon"></span>  <font color="white"><b>Jenis Proyek</b></font></a></li>
        <li><a href="karyawan-proyek.php"><span class="mif-users icon"></span>  <font color="white"><b>Karyawan Proyek</b></font></a></li>
        <li><a href="status-proyek.php"><span class="mif-cabinet icon"></span>  <font color="white"><b>Status Proyek</b></font></a></li>
        <?php
            }else{
        ?>
         <li class="menu-title" style="background:#004d40"> <font color="white"><b>Penilaian</b></font></li>
        <li  style="background:#004d40">
            <a href="#"><span class="mif-layers icon"></span> <font color="white"><b>Penilaian Kinerja</b></font></a>
            <ul class="v-menu" data-role="dropdown">
                <li><a href="kriteria.php">Kriteria</a></li>
                <li><a href="subkriteria.php">Sub Kriteria</a></li>
                <li><a href="alternatif.php">Alternatif</a></li>
                <li><a href="perangkingan.php">Perangkingan</a></li>
                <li><a href="cari-karyawan-proyek.php">Cari Perangkingan</a></li>
                <li><a href="grafik-penilaian.php" target="_blank">Grafik Penilaian</a></li>
                <li><a href="laporan.php" target="_blank">Laporan Penilaian</a></li>
            </ul>
        </li>   
        <li class="divider" style="background:#004d40"></li>
        <li class="menu-title" style="background:#004d40"> <font color="white"><b>Monitoring</b></font></li>
        <li><a href="data-proyek.php"><span class="mif-bookmark icon"></span>  <font color="white"><b>Data Proyek</b></font></a></li>
        <li><a href="kemajuan-data-proyek.php"><span class="mif-flag icon"></span>  <font color="white"><b>Kemajuan Proyek</b></font></a></li>
        <li><a href="jenis-proyek.php"><span class="mif-books icon"></span>  <font color="white"><b>Jenis Proyek</b></font></a></li>
        <li><a href="status-proyek.php"><span class="mif-cabinet icon"></span>  <font color="white"><b>Status Proyek</b></font></a></li>
        <li><a href="data-karyawan.php"><span class="mif-users icon"></span>  <font color="white"><b>Data Karyawan</b></font></a></li>
        <li><a href="karyawan-proyek.php"><span class="mif-users icon"></span>  <font color="white"><b>Karyawan Proyek</b></font></a></li>
        <li><a href="grafik-proyek.php"><span class="mif-chart-pie icon"></span>  <font color="white"><b>Grafik Proyek</b></font></a></li>
        <li><a href="laporan-proyek.php"><span class="mif-chart-line icon"></span>  <font color="white"><b>Laporan</b></font></a></li>
        
        <li class="divider" style="background:#004d40"></li>
        <li class="menu-title" style="background:#004d40"> <font color="white"><b>Pengguna</b></font></li>
        <li><a href="operator.php"><span class="mif-user icon"></span>  <font color="white"><b>Operator</b></font></a></li>
        <li><a href="ubahpassword.php"><span class="mif-key icon"></span>  <font color="white"><b>Ubah Password</b></font></a></li>
        <li><a href="logout.php"><span class="mif-cross icon"></span>  <font color="white"><b>Logout</b></font></a></li>
        <?php } ?>
    </ul>

</div>