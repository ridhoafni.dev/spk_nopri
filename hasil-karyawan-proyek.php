<?php
include "header.php";
$page = isset($_GET['page'])?$_GET['page']:"";
?>
<div class="row cells4">
	<div class="cell colspan2">
		<h3>Cari Data Karyawan Proyek</h3>
	</div>

	<div class="cell colspan2 align-right">
		<a href="karyawan-proyek.php" class="button info">Kembali</a>
	</div>
</div>
	<p></p>
	<?php
	if(isset($_POST['cari'])){

		$proyek 					= $_POST['proyek'];
        $karyawan 					= $_POST['karyawan'];
        
        $stmt_cari = $db->prepare(" SELECT
            smart_karyawan.*,
            smart_karyawan_proyek.*,
            smart_data_proyek.*
        FROM
            smart_karyawan_proyek
            INNER JOIN smart_data_proyek ON smart_data_proyek.id_data_proyek = smart_karyawan_proyek.proyek_id
            INNER JOIN smart_karyawan ON smart_karyawan_proyek.karyawan_id = smart_karyawan.id_karyawan
            WHERE smart_karyawan_proyek.karyawan_id=? AND smart_data_proyek.jenis_proyek=?");
        $stmt_cari->bindParam(1,$proyek);
        $stmt_cari->bindParam(2,$option_val);
        if($stmt_cari->execute()){
            ?>
        <script type="text/javascript">location.href='cari-karyawan-proyek.php'</script>
            <?php
        } else{
            ?>
        <script type="text/javascript">alert('Gagal menyimpan data')</script>
        <?php
        }		
	}
	if(isset($_POST['update'])){ ?>
				
				<!-- <script type="text/javascript">alert('<?php// $_GET['id']; ?>')</script> -->
				<?php
				
				$id_kemajuan_proyek 	    = $_GET['id'];				
				$id_data_proyek 			= $_POST['proyek'];
				$kemajuan 					= $_POST['kemajuan'];
				$tanggal 					= $_POST['tanggal'];
				$user_id 					= $_SESSION['username'];
			
				$stmt = $db->prepare("update smart_kemajuan_proyek set data_proyek_id=?, kemajuan=?, tanggal=? where id_kemajuan_proyek=?");

				$stmt->bindParam(1,$id_data_proyek);
				$stmt->bindParam(2,$kemajuan);
				$stmt->bindParam(3,$tanggal);
				$stmt->bindParam(4,$id_kemajuan_proyek);
				if($stmt->execute()){
					?>
					<script type="text/javascript">location.href='kemajuan-data-proyek.php'</script>
					<?php
				} else{
					?>
					<script type="text/javascript">alert("Gagal Menyimpan data")</script>
					<?php
				}
		}
	?>
	<!-- Load File javascript config.js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/config.js" type="text/javascript"></script>
	<form method="post">
		<input type="hidden" name="id_kemajuan_proyek" value="<?php echo isset($_GET['id'])? $_GET['id'] : ''; ?>">
        <?php
            //Get all country data
            $query = $db->prepare("select * from smart_data_proyek ORDER BY nama_proyek ASC");
            
            //Count total number of rows
            $query->execute();

            $rowCount = $query->fetch();
        ?>
		<label>Jenis Proyek</label>
		<div class="input-control text full-size">
			<select name="proyek">
		    	<option value="<?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?>"><?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?></option>
		    	<?php
				$stmt3 = $db->prepare("select * from smart_data_proyek");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
		    	<option value="<?php echo $row3['jenis_proyek'] ?>"><?php echo $row3['jenis_proyek'] ?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</div>

        <label>Karyawan</label>
		<div class="input-control text full-size">
			<select name="karyawan">
		    	<option value="<?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?>"><?php echo isset($_GET['alt'])? $_GET['alt'] : ''; ?></option>
		    	<?php
				$stmt3 = $db->prepare("select * from smart_karyawan");
				$stmt3->execute();
				while($row3 = $stmt3->fetch()){
				?>
		    	<option value="<?php echo $row3['id_karyawan'] ?>"><?php echo $row3['nama_karyawan'] ?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</div>

		<?php
		if (isset($_GET['id'])) {
			?>
			<button type="submit" name="update" class="button warning">Update</button>
			<?php
		} else{
			?>
			<button type="submit" name="cari" class="button primary">Cari</button>
			<?php
		}
		?>
	</form>

</div>
<?php
include "footer.php";
?>
				